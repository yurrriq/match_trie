((nil
  . ((indent-tabs-mode . nil)
     (fill-column      . 80)
     (eval             . (turn-on-fci-mode))))
 (erlang-mode
  . ((erlang-indent-level . 2)
     (flycheck-erlang-include-path . ("../include")))))
