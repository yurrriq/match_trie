match_trie
=====

a simple Erlang Trie implementation using ETS. This module provides
a simple way to match paths or routing keys.


Build
-----

    $ rebar3 compile
